namespace Models
{
    public class ProductT<T>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }
        /// <summary>
        /// LeadTime is measured in days
        /// </summary>
        public int LeadTime { get; set; }
        public T Properties { get; set; }
    }
}