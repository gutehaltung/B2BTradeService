namespace Models
{
    public class ProductTypes
    {
        public class Generator
        {
            public int Power { get; set; }
            public int Width { get; set; }
            public int Length { get; set; }
            public int Height { get; set; }
            public int Weight { get; set; }
        }
    }
}