import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartIconComponent } from './cart-icon/cart-icon.component';
import { CartViewComponent } from './cart-view/cart-view.component';
import {CatalogRoutingModule} from "./catalog-routing.module";
import { ProductListComponent } from './product-list/product-list.component';
import { ProductViewComponent } from './product-view/product-view.component';
import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";



@NgModule({
  declarations: [
    CartIconComponent,
    CartViewComponent,
    ProductListComponent,
    ProductViewComponent
  ],
  imports: [
    CommonModule,
    CatalogRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ]
})
export class CatalogModule { }
