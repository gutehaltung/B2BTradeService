import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from "@angular/forms";
import { ProductsService } from "../../../services/products.service";
import Product from "../../../models/Product";

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  private initProductList: Product[] = [];
  productList: Product[] = [];
  loading = false;
  filter = new FormGroup({ inStock: new FormControl(false) });

  private productService: ProductsService;

  constructor(productService: ProductsService) {
    this.productService = productService;
  }

  ngOnInit(): void {
    this.loading = true;
    setTimeout(() => {
      this.productService.getProductList.subscribe((value) => {
        this.initProductList = value;
        this.productList = value;
      });
      this.loading = false;
    }, 1500)
  }

  filterProduct(): void {
    this.productList = this.initProductList?.filter(prod => this.filter.value['inStock'] ? prod.LeadTime === 0 : true);
  }

}
