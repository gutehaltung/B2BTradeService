import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServiceRoutingModule } from "./service-routing.module";
import { ProductFinderComponent } from './product-finder/product-finder.component';
import { DocumentViewComponent } from './document-view/document-view.component';



@NgModule({
  declarations: [
    ProductFinderComponent,
    DocumentViewComponent
  ],
  imports: [
    CommonModule,
    ServiceRoutingModule
  ]
})
export class ServiceModule { }
