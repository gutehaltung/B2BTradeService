import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { routePaths } from "../../shared/route-paths";
import { ProductFinderComponent } from "./product-finder/product-finder.component";
import { DocumentViewComponent } from "./document-view/document-view.component";


const routes: Routes = [
  { path: routePaths.service, component: ProductFinderComponent },
  { path: routePaths.documents, component: DocumentViewComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceRoutingModule {}
