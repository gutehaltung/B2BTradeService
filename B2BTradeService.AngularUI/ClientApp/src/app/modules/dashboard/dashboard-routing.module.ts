import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { routePaths } from "../../shared/route-paths";
import { AboutComponent } from "./about/about.component";

const routes: Routes = [
  { path: routePaths.about, component: AboutComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {}
