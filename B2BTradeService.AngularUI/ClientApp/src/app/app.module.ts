import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule} from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './shared/footer/footer.component';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';
import { CatalogModule } from "./modules/catalog/catalog.module";
import { ServiceModule } from "./modules/service/service.module";
import { DashboardModule } from "./modules/dashboard/dashboard.module";
import { NavComponent } from './shared/nav/nav.component';
import { AuthorizationComponent } from './authorization/authorization.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    FooterComponent,
    PageNotFoundComponent,
    NavComponent,
    AuthorizationComponent,
  ],
  imports: [
    CatalogModule,
    ServiceModule,
    DashboardModule,
    BrowserModule,
    RouterModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
