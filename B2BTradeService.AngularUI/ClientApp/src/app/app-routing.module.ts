import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from "./shared/page-not-found/page-not-found.component";
import { routePaths } from "./shared/route-paths";
import { AuthorizationComponent } from "./authorization/authorization.component";

const routes: Routes = [
  { path: routePaths.authorization, component: AuthorizationComponent },
  { path: "", redirectTo: routePaths.authorization, pathMatch: "full" },
  { path: "**", component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
