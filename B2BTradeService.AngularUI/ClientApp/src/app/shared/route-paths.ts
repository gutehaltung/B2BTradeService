export const routePaths = {
  authorization: "authorization",
  about: "about",
  catalog: "catalog",
  service: "service",
  documents: "documents",
  contacts: "contacts",
  profile: "profile"
}
