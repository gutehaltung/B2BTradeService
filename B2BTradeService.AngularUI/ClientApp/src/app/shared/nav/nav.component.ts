import { Component, OnInit } from '@angular/core';
import { routePaths } from "../route-paths";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],

})
export class NavComponent implements OnInit {

  routePaths = routePaths;

  constructor() { }

  ngOnInit(): void {
  }

}
