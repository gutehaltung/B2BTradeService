import { IProduct } from "./Interfaces/IProduct";

export default class Product implements IProduct {
  Id: number;
  Name: string;
  LeadTime: number;
  Price: number;

  constructor(id: number, name: string, leadTime: number, price: number) {
    this.Id = id;
    this.Name = name;
    this.LeadTime = leadTime;
    this.Price = price;
  }
}
