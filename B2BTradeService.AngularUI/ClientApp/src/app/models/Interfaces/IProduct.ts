export interface IProduct {
  Id: number;
  Name: string;
  LeadTime: number;
  Price: number;
}
