export default interface IResult<T> {
  isSuccess: boolean,
  data: T,
  statusCode: number,
}
