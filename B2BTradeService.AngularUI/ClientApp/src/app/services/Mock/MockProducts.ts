import Product from "../../models/Product";

export const MockProducts = [
  new Product(1, "Corporis molestias optio voluptates voluptatum?", 100, 250),
  new Product(2, "Lorem ipsum dolor sit amet.", 1000, 1000),
  new Product(3, "Ipsam modi quasi quos voluptate.", 0, 10),
  new Product(4, "Cum cupiditate deleniti labore minima.", 30, 100),
  new Product(5, "Debitis laborum nobis praesentium reprehenderit.", 300, 500),
]
