import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { MockProducts } from "./Mock/MockProducts";
import { catchError, map, shareReplay } from "rxjs/operators";
import Product from "../models/Product";
import { IProduct } from "../models/Interfaces/IProduct";

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) { }

  get getProductList(): Observable<Product[]> {
    return this.http.get<IProduct[]>("assets/mock-products.json").pipe(
      catchError(this.handleError),
      map((data: any) => {
        let products = data["products"];
        return products.map(function(prod: IProduct): Product {
          return new Product(prod.Id, prod.Name, prod.LeadTime, prod.Price);
        })
      }),
      shareReplay());
  }

  ProductList(): Observable<Product[]> {
    return this.http.get<IProduct[]>("assets/mock-products.json").pipe(
      catchError(this.handleError),
      map((data: any) => {
        let products = data["products"];
        return products.map(function(prod: IProduct): Product {
          return new Product(prod.Id, prod.Name, prod.LeadTime,  prod.Price);
        })
      }));
  }

  private handleError(error: HttpErrorResponse) {
    return throwError("Something goes wrong");
  }
}
