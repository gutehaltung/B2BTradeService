﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace B2BTradeService.AngularUI.Models
{
    public class ApplicationUser : IdentityUser
    {
    }
}